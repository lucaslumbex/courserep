//
//  GetDepartmentsResponse.swift
//  CourseRep
//
//  Created by lucas lumbex on 14/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation

// MARK: - GetDepartmentsResponse
struct GetDepartmentsResponse: Codable {
    let responsecode, responsemessage: String?
    let departments: [Department]?
}

// MARK: - Department
struct Department: Codable {
    let deptabbrv, code, maxlevel, dateupdated: String?
    let updateaction, name, uniqueref, id: String?
    let datecreated, faculty: String?
}
