//
//  CustomerStatusResponse.swift
//  CourseRep
//
//  Created by lucas lumbex on 14/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation

// MARK: - CustomerStatusResponse
struct CustomerStatusResponse: Codable {
    let responsecode, firstname, gender, dob: String?
    let university, responsemessage, lastlogindate, phonenumber: String?
    let emailaddress, matricno, department, lastname: String?
}
