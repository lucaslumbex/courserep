//
//  EnrolmentCompleteResponse.swift
//  CourseRep
//
//  Created by lucas lumbex on 13/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation

// MARK: - EnrolmentCompleteResponse
struct EnrolmentCompleteResponse: Codable {
    let responsecode, firstname, gender, level: String?
    let university, responsemessage, uniqueref, phonenumber: String?
    let otp, emailaddress, matricno, source: String?
    let lastname, pushid, password, dob: String?
    let referallink, department: String?
}
