//
//  LoginResponse.swift
//  CourseRep
//
//  Created by lucas lumbex on 13/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation

// MARK: - LoginResponse
struct LoginResponse: Codable {
    let responsecode, firstname, gender, university: String?
    let responsemessage, lastlogindate, phonenumber, emailaddress: String?
    let source, matricno, lastname, pushid: String?
    let dob, department: String?
}

