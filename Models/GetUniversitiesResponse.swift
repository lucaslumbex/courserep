//
//  GetUniversitiesResponse.swift
//  CourseRep
//
//  Created by lucas lumbex on 14/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation

// MARK: - GetUniversitiesResponse
struct GetUniversitiesResponse: Codable {
    let responsecode: String?
    let universities: [University]?
    let responsemessage: String?
}

// MARK: - University
struct University: Codable {
    let abbrv, code, address, dateupdated: String?
    let updateaction, name, founded, uniqueref: String?
    let id, state, datecreated, type: String?
}
