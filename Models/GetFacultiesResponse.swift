//
//  GetFacultiesResponse.swift
//  CourseRep
//
//  Created by lucas lumbex on 14/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation

// MARK: - GetFacultiesResponse
struct GetFacultiesResponse: Codable {
    let responsecode, responsemessage: String?
    let faculties: [Faculty]?
}

// MARK: - Faculty
struct Faculty: Codable {
    let dean, code, dateupdated, updateaction: String?
    let university, name, uniqueref, subdean: String?
    let id, datecreated: String?
}
