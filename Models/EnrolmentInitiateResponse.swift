//
//  EnrolmentInitiateResponse.swift
//  CourseRep
//
//  Created by lucas lumbex on 13/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation

// MARK: - EnrolmentInitiateResponse
struct EnrolmentInitiateResponse: Codable {
    let responsecode, firstname, gender, level: String?
    let university, responsemessage, phonenumber, uniqueref: String?
    let emailaddress, matricno, source, lastname: String?
    let dob, department: String?
}
