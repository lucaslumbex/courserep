//
//  AuthViewModel.swift
//  CourseRep
//
//  Created by lucas lumbex on 14/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation

class AuthViewModel:BaseViewModel {
    // MARK: - Properties
    private var enrolmentInitiateResponse : EnrolmentInitiateResponse?{
        didSet {
            guard let response = enrolmentInitiateResponse else { return }
            self.setupText(with: response)
            self.didFinishFetch?()
        }
    }
    private var enrolmentCompleteResponse : EnrolmentCompleteResponse?{
        didSet {
            guard let response = enrolmentCompleteResponse else { return }
            self.setupText(with: response)
            self.didFinishFetch?()
        }
    }

    private var loginResponse : LoginResponse?{
        didSet {
            guard let response = loginResponse else { return }
            self.setupText(with: response)
            self.didFinishFetch?()
        }
    }

    // MARK: - UI logic
    private func setupText(with enrolmentInitiateResponse: EnrolmentInitiateResponse){
        
    }
    private func setupText(with enrolmentCompleteResponse: EnrolmentCompleteResponse){
        
    }
    private func setupText(with loginResponse: LoginResponse){
        
    }
    
    
    
    
    // MARK: - Network Call (actual API call) -->> enrolmentInitiate
    func enrolmentInitiate(university:String, department:String, level:String, gender: String, dob:String, matricno:String, source:String, firstname:String, lastname:String, emailaddress:String, phonenumber:String) {
        
        self.isLoading = true
        
        self.dataService?.enrolmentInitiate(university: university, department: department, level: level, gender: gender, dob: dob, matricno: matricno, source: source, firstname: firstname, lastname: lastname, emailaddress: emailaddress, phonenumber: phonenumber,  completion: {(enrolmentInitiateResponse, error)
            in
            if let error = error{
                self.error = error
                self.isLoading = false
                
                return
            }
            self.isLoading = false
            
            switch(enrolmentInitiateResponse?.responsecode){
            case "00" :
                self.enrolmentInitiateResponse = enrolmentInitiateResponse
                break
            default:
                self.apiError = enrolmentInitiateResponse?.responsemessage
                break
                
            }
        })
    }
    
    // MARK: - Network Call (actual API call) -->> enrolmentComplete
    func enrolmentComplete(university:String, department:String, gender: String, dob:String, matricno:String, source:String, firstname:String, lastname:String, emailaddress:String, phonenumber:String, uniqueref:String, referallink:String, pushid:String, otp:String, password:String) {
        
        self.isLoading = true
        
        self.dataService?.enrolmentComplete(university:university, department:department, gender:gender, dob:dob, matricno:matricno, source:source, firstname:firstname, lastname:lastname, emailaddress:emailaddress, phonenumber:phonenumber, uniqueref:uniqueref, referallink:referallink, pushid:pushid, otp:otp, password:password, completion: {(enrolmentCompleteResponse, error)
            in
            if let error = error{
                self.error = error
                self.isLoading = false
                
                return
            }
            self.isLoading = false
            
            switch(enrolmentCompleteResponse?.responsecode){
            case "00" :
                self.enrolmentCompleteResponse = enrolmentCompleteResponse
                break
            default:
                self.apiError = enrolmentCompleteResponse?.responsemessage
                break
                
            }
        })
    }
    
    
    // MARK: - Network Call (actual API call) -->> login
    func login(source:String, password:String, emailaddress: String, pushid:String) {
        
        self.isLoading = true
        
        self.dataService?.login(source:source, password:password, emailaddress:emailaddress, pushid:pushid,  completion: {(loginResponse, error)
            in
            if let error = error{
                self.error = error
                self.isLoading = false
                
                return
            }
            self.isLoading = false
            
            switch(loginResponse?.responsecode){
            case "00" :
                self.loginResponse = loginResponse
                break
            default:
                self.apiError = loginResponse?.responsemessage
                break
                
            }
        })
    }
    
    
    
    
}
