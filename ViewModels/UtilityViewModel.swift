//
//  UtilityViewModel.swift
//  CourseRep
//
//  Created by lucas lumbex on 14/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation


class UtilityViewModel:BaseViewModel {
    // MARK: - Properties
    private var customerStatusResponse : CustomerStatusResponse?{
        didSet {
            guard let response = customerStatusResponse else { return }
            self.setupText(with: response)
            self.didFinishFetch?()
        }
    }
    private var getUniversitiesResponse : GetUniversitiesResponse?{
        didSet {
            guard let response = getUniversitiesResponse else { return }
            self.setupText(with: response)
            self.didFinishFetch?()
        }
    }
    
    private var getFacultiesResponse : GetFacultiesResponse?{
        didSet {
            guard let response = getFacultiesResponse else { return }
            self.setupText(with: response)
            self.didFinishFetch?()
        }
    }
    
    private var getDepartmentsResponse : GetDepartmentsResponse?{
        didSet {
            guard let response = getDepartmentsResponse else { return }
            self.setupText(with: response)
            self.didFinishFetch?()
        }
    }
    
    // MARK: - UI logic
    private func setupText(with customerStatusResponse: CustomerStatusResponse){
        
    }
    private func setupText(with getUniversitiesResponse: GetUniversitiesResponse){
        
    }
    private func setupText(with getFacultiesResponse: GetFacultiesResponse){
        
    }
    private func setupText(with getDepartmentsResponse: GetDepartmentsResponse){
        
    }

    
    
    
    // MARK: - Network Call (actual API call) -->> customerStatus
    func customerStatus(source:String, emailaddress: String, pushid:String) {
        
        self.isLoading = true
        
        self.dataService?.customerStatus(source: source, emailaddress: emailaddress, pushid: pushid,  completion: {(customerStatusResponse, error)
            in
            if let error = error{
                self.error = error
                self.isLoading = false
                
                return
            }
            self.isLoading = false
            
            switch(customerStatusResponse?.responsecode){
            case "00" :
                self.customerStatusResponse = customerStatusResponse
                break
            default:
                self.apiError = customerStatusResponse?.responsemessage
                break
                
            }
        })
    }
    
    
    // MARK: - Network Call (actual API call) -->> getUniversities
    func getUniversities(source:String, emailaddress: String, pushid:String) {
        
        self.isLoading = true
        
        self.dataService?.getUniversities(source: source, emailaddress: emailaddress, pushid: pushid,  completion: {(getUniversitiesResponse, error)
            in
            if let error = error{
                self.error = error
                self.isLoading = false
                
                return
            }
            self.isLoading = false
            
            switch(getUniversitiesResponse?.responsecode){
            case "00" :
                self.getUniversitiesResponse = getUniversitiesResponse
                break
            default:
                self.apiError = getUniversitiesResponse?.responsemessage
                break
                
            }
        })
    }
    
    
    // MARK: - Network Call (actual API call) -->> getFaculties
    func getFaculties(source:String, emailaddress: String, _universitycode:String, comment:String) {
        
        self.isLoading = true
        
        self.dataService?.getFaculties(source: source, emailaddress: emailaddress, _universitycode: _universitycode, comment: comment, completion: {(getFacultiesResponse, error)
            in
            if let error = error{
                self.error = error
                self.isLoading = false
                
                return
            }
            self.isLoading = false
            
            switch(getFacultiesResponse?.responsecode){
            case "00" :
                self.getFacultiesResponse = getFacultiesResponse
                break
            default:
                self.apiError = getFacultiesResponse?.responsemessage
                break
                
            }
        })
    }
    
    
    // MARK: - Network Call (actual API call) -->> getDepartments
    func getDepartments(source:String, emailaddress:String, facultycode:String, comment:String) {
        
        self.isLoading = true
        
        self.dataService?.getDepartments(source: source, emailaddress: emailaddress, facultycode: facultycode, comment: comment, completion: {(getDepartmentsResponse, error)
            in
            if let error = error{
                self.error = error
                self.isLoading = false
                
                return
            }
            self.isLoading = false
            
            switch(getDepartmentsResponse?.responsecode){
            case "00" :
                self.getDepartmentsResponse = getDepartmentsResponse
                break
            default:
                self.apiError = getDepartmentsResponse?.responsemessage
                break
                
            }
        })
    }
}

