//
//  BaseViewModel.swift
//  CourseRep
//
//  Created by lucas lumbex on 14/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation
class BaseViewModel {
    
    //    from here down is generic to all view models
    var error: Error? {
        didSet { self.showAlertClosure?() }
    }
    
    var apiError: String? = "Something went wrong" {
        didSet { self.showAlertClosure?() }
    }
    
    var isLoading: Bool = false {
        didSet { self.updateLoadingStatus?() }
    }
    
    var dataService: DataService?
    
    
    // MARK: - Closures for callbacks since we are not using viewmodels for the view
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var didFinishFetch: (() -> ())?
    
    
    // MARK: - Constuctor
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    //    from here down is generic to all view models  END
    
}
