//
//  DataService.swift
//  CourseRep
//
//  Created by lucas lumbex on 13/08/2019.
//  Copyright © 2019 lucas lumbex. All rights reserved.
//

import Foundation
import Alamofire

struct DataService {
    
    //MARK SINGLETON
    static let shared = DataService()
    
    //HEADERS
    let headers: HTTPHeaders = ["content-Type" :"application/json"]
    
    // MARK: -URL
    private let baseURL = "https://0nto6g2q1a.execute-api.us-west-2.amazonaws.com/dev/"
    private let enrolmentInitiateUrl = "enrolmentinitiate"
    private let enrolmentCompleteUrl = "enrolmentcomplete"
    private let loginUrl = "login"
    private let customerStatusUrl = "customerstatus"
    private let getUniversitiesUrl = "getuniversities"
    private let getFacultiesUrl = "getfaculties"
    private let getDepartmentsUrl = "getdepartments"
    
    
    func enrolmentInitiate(university:String, department:String, level:String, gender: String, dob:String, matricno:String, source:String, firstname:String, lastname:String, emailaddress:String, phonenumber:String, completion: @escaping (EnrolmentInitiateResponse?, Error?)-> ())  {
        let parameters: [String:Any] = ["university": university, "department": department, "level": level, "gender": gender, "dob": dob, "matricno": matricno, "source": source, "firstname": firstname, "lastname": lastname, "emailaddress": emailaddress, "phonenumber": phonenumber]
        AF.request(baseURL + enrolmentInitiateUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON { (response:DataResponse<Any>) in
                print(parameters)
                print(response)
                switch(response.result){
                case.success(_):
                    do {
                        //here dataResponse recieved from a network
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(EnrolmentInitiateResponse.self, from:
                            response.data!) //Decode Json Response Data
                        completion(model, nil)
                    } catch let parsingError {
                        print("Error", parsingError)
                        completion(nil, parsingError)
                    }
                    break
                case.failure(_):
                    print(response.error as Any)
                    completion(nil,response.error)
                    break
                }
        }
    }
    
    func enrolmentComplete(university:String, department:String, gender: String, dob:String, matricno:String, source:String, firstname:String, lastname:String, emailaddress:String, phonenumber:String, uniqueref:String, referallink:String, pushid:String, otp:String, password:String, completion: @escaping (EnrolmentCompleteResponse?, Error?)-> ())  {
        let parameters: [String:Any] = ["university": university, "department": department, "gender": gender, "dob": dob, "matricno": matricno, "source": source, "firstname": firstname, "lastname": lastname, "emailaddress": emailaddress, "phonenumber": phonenumber, "uniqueref":uniqueref, "pushid":pushid, "otp":otp, "password":password]
        AF.request(baseURL + enrolmentCompleteUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON { (response:DataResponse<Any>) in
                print(parameters)
                print(response)
                switch(response.result){
                case.success(_):
                    do {
                        //here dataResponse recieved from a network
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(EnrolmentCompleteResponse.self, from:
                            response.data!) //Decode Json Response Data
                        completion(model, nil)
                    } catch let parsingError {
                        print("Error", parsingError)
                        completion(nil, parsingError)
                    }
                    break
                case.failure(_):
                    print(response.error as Any)
                    completion(nil,response.error)
                    break
                }
        }
    }
    
    func login(source:String, password:String, emailaddress: String, pushid:String, completion: @escaping (LoginResponse?, Error?)-> ())  {
        let parameters: [String:Any] = ["source": source, "password": password, "emailaddress": emailaddress, "pushid": pushid]
        AF.request(baseURL + loginUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON { (response:DataResponse<Any>) in
                print(parameters)
                print(response)
                switch(response.result){
                case.success(_):
                    do {
                        //here dataResponse recieved from a network
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(LoginResponse.self, from:
                            response.data!) //Decode Json Response Data
                        completion(model, nil)
                    } catch let parsingError {
                        print("Error", parsingError)
                        completion(nil, parsingError)
                    }
                    break
                case.failure(_):
                    print(response.error as Any)
                    completion(nil,response.error)
                    break
                }
        }
    }
    
    func customerStatus(source:String, emailaddress: String, pushid:String, completion: @escaping (CustomerStatusResponse?, Error?)-> ())  {
        let parameters: [String:Any] = ["source": source, "emailaddress": emailaddress, "pushid": pushid]
        AF.request(baseURL + customerStatusUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON { (response:DataResponse<Any>) in
                print(parameters)
                print(response)
                switch(response.result){
                case.success(_):
                    do {
                        //here dataResponse recieved from a network
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(CustomerStatusResponse.self, from:
                            response.data!) //Decode Json Response Data
                        completion(model, nil)
                    } catch let parsingError {
                        print("Error", parsingError)
                        completion(nil, parsingError)
                    }
                    break
                case.failure(_):
                    print(response.error as Any)
                    completion(nil,response.error)
                    break
                }
        }
    }
    
    func getUniversities(source:String, emailaddress: String, pushid:String, completion: @escaping (GetUniversitiesResponse?, Error?)-> ())  {
        let parameters: [String:Any] = ["source": source, "emailaddress": emailaddress, "pushid": pushid]
        AF.request(baseURL + getUniversitiesUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON { (response:DataResponse<Any>) in
                print(parameters)
                print(response)
                switch(response.result){
                case.success(_):
                    do {
                        //here dataResponse recieved from a network
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(GetUniversitiesResponse.self, from:
                            response.data!) //Decode Json Response Data
                        completion(model, nil)
                    } catch let parsingError {
                        print("Error", parsingError)
                        completion(nil, parsingError)
                    }
                    break
                case.failure(_):
                    print(response.error as Any)
                    completion(nil,response.error)
                    break
                }
        }
    }
    
    func getFaculties(source:String, emailaddress: String, _universitycode:String, comment:String, completion: @escaping (GetFacultiesResponse?, Error?)-> ())  {
        let parameters: [String:Any] = ["source": source, "emailaddress": emailaddress, "_universitycode": _universitycode, comment:comment]
        AF.request(baseURL + getFacultiesUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON { (response:DataResponse<Any>) in
                print(parameters)
                print(response)
                switch(response.result){
                case.success(_):
                    do {
                        //here dataResponse recieved from a network
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(GetFacultiesResponse.self, from:
                            response.data!) //Decode Json Response Data
                        completion(model, nil)
                    } catch let parsingError {
                        print("Error", parsingError)
                        completion(nil, parsingError)
                    }
                    break
                case.failure(_):
                    print(response.error as Any)
                    completion(nil,response.error)
                    break
                }
        }
    }
    
    
    func getDepartments(source:String, emailaddress:String, facultycode:String, comment:String, completion: @escaping (GetDepartmentsResponse?, Error?)-> ())  {
        let parameters: [String:Any] = ["source": source, "emailaddress": emailaddress, "facultycode":facultycode, "comment": comment]
        AF.request(baseURL + getDepartmentsUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON { (response:DataResponse<Any>) in
                print(parameters)
                print(response)
                switch(response.result){
                case.success(_):
                    do {
                        //here dataResponse recieved from a network
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(GetDepartmentsResponse.self, from:
                            response.data!) //Decode Json Response Data
                        completion(model, nil)
                    } catch let parsingError {
                        print("Error", parsingError)
                        completion(nil, parsingError)
                    }
                    break
                case.failure(_):
                    print(response.error as Any)
                    completion(nil,response.error)
                    break
                }
        }
    }
    
    
}
